<?php include "../inc/koneksi.php"; 

?>
<!DOCTYPE html>
<html>

<head> 

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="/image/png" href="../img/logo kps 2.png">

    <title>Business Center SMK Negeri 1 Baso</title>

    <!-- Core CSS - Include with every page -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Kepala Sekolah</a>
            </div>
            <!-- /.navbar-header -->

            

             <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                        <li>
                            <a href="changepass.php?id=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-edit fa-fw"></i>Tukar Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="index.php?a=logout"><i class="fa fa-sign-out fa-fw"></i>Keluar</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Laporan Penjualan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">

                
                <div class="col-lg-12">
                
                <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Laporan Penjualan
                        </div>
                        <!-- /.panel-heading -->

 						
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Nama Barang 1</th>
                                            <th>Jumlah</th>
                                            <th>Nama Barang 2</th>
                                            <th>Jumlah</th>
                                            <th>Nama Barang 3</th>
                                            <th>Jumlah</th>
                                            <th>Nama Barang 4</th>
                                            <th>Jumlah</th>
                                            <th>Nama Barang 5</th>
                                            <th>Jumlah</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    if (isset($_POST['submit'])) {

                                         $tahun=$_POST['tahun'];
                                    $sql = mysql_query("SELECT * FROM penjualan where DATE_FORMAT(waktu,'%Y')='".mysql_real_escape_string($tahun)."'");
                                    while ($data = mysql_fetch_array($sql)) { 
                                    
                                        ?>
                                
                                        <tr>
                                         <td><?php echo $data['waktu'] ?></td>
                                            <td><?php echo $data['nama_barang1'] ?></td>
                                            <td><?php echo $data['jumlah1'] ?></td>
                                            <td><?php echo $data['nama_barang2'] ?></td>
                                            <td><?php echo $data['jumlah2']?></td>
                                            <td><?php echo $data['nama_barang3']?></td>   
                                            <td><?php echo $data['jumlah3']?></td>
                                            <td><?php echo $data['nama_barang4']?></td>
                                            <td><?php echo $data['jumlah4']?></td>
                                            <td><?php echo $data['nama_barang5']?></td>
                                            <td><?php echo $data['jumlah5']?></td>
                                            <td><?php echo $data['bayar']?></td>
                                        </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                     <a href="print.php" target="_blank" class="btn btn-ksa">Print</a>
                            </div>
                            

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>