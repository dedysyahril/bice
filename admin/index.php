<?php include "inc/koneksi.php"; 
if (isset($_GET['a']) && $_GET['a'] == 'logout') {
    unset($_SESSION['user_id']);
}
if (!isset($_SESSION['user_id'])&& empty($_SESSION['user_id'])) {
  header ("location:../login.html");
}
?>
<!DOCTYPE html>
<html>
 
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="/image/png" href="../img/logo kps 2.png">

    <title>Business Center</title>

    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Admin Area</a>
            </div>
            <!-- /.navbar-header -->

            

             <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                            <a href="listsuplier.php"><i class="fa fa-edit fa-fw"></i>Suplier</a>
                        </li>
                            <li>
                            <a href="create_cat.php"><i class="fa fa-edit fa-fw"></i> Membuat Kategori</a>
                        </li>
                        <li>
                            <a href="inputbarang.php"><i class="fa fa-edit fa-fw"></i> Input Barang</a>
                        </li>
                        <li>
                            <a href="listbarang.php"><i class="fa fa-edit fa-fw"></i> listbarang</a>
                        </li>
                        <li>
                            <a href="listmasuk.php"><i class="fa fa-edit fa-fw"></i> Laporan Barang Masuk</a>
                        </li>
                        <li>
                         <a href="users.php"><i class="fa fa-table fa-fw"></i> List User</a>
                        </li>
                        <li>
                            <a href="changepass.php?id=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-edit fa-fw"></i>Tukar Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="index.php?a=logout"><i class="fa fa-sign-out fa-fw"></i>Keluar</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                 </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="jumbotron">
                        <h1>Selamat Datang</h1>
                        <p>Selamat datang wahai admin business center SMK Negeri 1 Baso Kab. Agam. </p>
                        </p>
                    </div>
                </div>

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
