-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2015 at 09:24 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bice`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangmasuk`
--

CREATE TABLE IF NOT EXISTS `barangmasuk` (
  `id` int(4) NOT NULL,
  `tanggal` date NOT NULL,
  `barcode` varchar(32) NOT NULL,
  `nama_barang` varchar(32) NOT NULL,
  `suplier` varchar(32) NOT NULL,
  `jumlah` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barangmasuk`
--

INSERT INTO `barangmasuk` (`id`, `tanggal`, `barcode`, `nama_barang`, `suplier`, `jumlah`) VALUES
(10, '2015-07-09', '101', 'baju putih perempuan', 'Maju Jaya', 50),
(11, '2015-07-09', '102', 'baju putih laki-laki', 'Maju Jaya', 50),
(13, '2015-07-09', '103', 'baju pramuka perempuan', 'Maju Jaya', 50),
(14, '2015-07-09', '104', 'baju pramuka laki-laki', 'Maju Jaya', 50),
(15, '2015-07-09', '105', 'baju batik perempuan', 'Maju Jaya', 50),
(16, '2015-07-09', '106', 'baju batik laki-laki', 'Maju Jaya', 50),
(17, '2015-07-09', '107', 'baju kurung', 'Maju Jaya', 50),
(18, '2015-07-09', '108', 'baju koko', 'Maju Jaya', 50),
(19, '2015-07-09', '109', 'rok abu-abu', 'Maju Jaya', 50),
(20, '2015-07-09', '110', 'celana abu-abu', 'Maju Jaya', 50),
(24, '2015-07-09', '111', 'rok pramuka', 'Maju Jaya', 50),
(25, '2015-07-09', '112', 'celana pramuka', 'Maju Jaya', 50),
(26, '2015-07-09', '113', 'rok hitam', 'Maju Jaya', 50),
(27, '2015-07-09', '114', 'celana hitam', 'Maju Jaya', 50),
(28, '2015-07-09', '115', 'jilbab putih', 'Maju Jaya', 50),
(29, '2015-07-09', '116', 'jilbab pramuka', 'Maju Jaya', 50),
(30, '2015-07-09', '201', 'topi abu-abu', 'Maju Jaya', 100),
(31, '2015-07-09', '202', 'dasi abu-abu', 'Maju Jaya', 100),
(32, '2015-07-09', '203', 'kacu', 'Maju Jaya', 55),
(33, '2015-07-09', '204', 'topi pamuka', 'Maju Jaya', 67),
(34, '2015-07-09', '205', 'peci', 'Maju Jaya', 60),
(35, '2015-07-09', '206', 'tali pramuka', 'Maju Jaya', 56),
(36, '2015-07-09', '207', 'lambang jurusan perbankan', 'Maju Jaya', 45),
(37, '2015-07-09', '208', 'lambang jurusan akuitansi', 'Maju Jaya', 67),
(39, '2015-07-09', '209', 'lambang jurusan tkj', 'Maju Jaya', 56),
(40, '2015-07-09', '210', 'lambang jurusan administrasi', 'Maju Jaya', 67),
(42, '2015-07-09', '211', 'bintang kelas 1', 'Maju Jaya', 67),
(43, '2015-07-09', '212', 'bintang kelas II', 'Maju Jaya', 56),
(44, '2015-07-09', '213', 'bintang kelas III', 'Maju Jaya', 45),
(45, '2015-07-11', '0', '0', 'Sederhana', 0),
(46, '2015-07-13', '101', 'seragam putih perempuan', 'Maju Jaya', 50),
(47, '2015-07-29', '101', 'seragam putih perempuan', 'Maju Jaya', 10);

-- --------------------------------------------------------

--
-- Table structure for table `detail`
--

CREATE TABLE IF NOT EXISTS `detail` (
  `nota` int(3) NOT NULL,
  `id_produk` int(15) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `subtotal` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id_kategori` int(15) NOT NULL,
  `nama_kategori` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Seragam'),
(2, 'Alat Tulis'),
(3, 'Makanan'),
(4, 'Minuman'),
(5, 'Atribut Seragam'),
(6, 'dll');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `nota` int(3) NOT NULL,
  `waktu` date NOT NULL,
  `pembayaran` int(16) NOT NULL,
  `kasir` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id_produk` int(15) NOT NULL,
  `suplier` varchar(16) NOT NULL,
  `kategori` varchar(15) NOT NULL,
  `nama_produk` varchar(25) NOT NULL,
  `deskripsi` varchar(30) NOT NULL,
  `beli` varchar(16) NOT NULL,
  `jual` varchar(16) NOT NULL,
  `stok` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `suplier`, `kategori`, `nama_produk`, `deskripsi`, `beli`, `jual`, `stok`) VALUES
(0, 'Sederhana', 'Seragam', '0', '0', '1000', '1100', 9),
(101, 'Maju Jaya', 'Seragam', 'seragam putih perempuan', 'seragam sekolah', '35000', '40000', 60),
(102, 'Maju Jaya', 'Seragam', 'baju putih laki-laki', 'seragam sekolah', '30000', '35000', 50),
(103, 'Maju Jaya', 'Seragam', 'baju pramuka perempuan', 'seragam sekolah', '35000', '40000', 47),
(104, 'Maju Jaya', 'Seragam', 'baju pramuka laki-laki', 'seragam sekolah', '30000', '35000', 50),
(105, 'Maju Jaya', 'Seragam', 'baju batik perempuan', 'seragam sekolah', '35000', '40000', 50),
(106, 'Maju Jaya', 'Seragam', 'baju batik laki-laki', 'seragam sekolah', '30000', '35000', 50),
(107, 'Maju Jaya', 'Seragam', 'baju kurung', 'seragam sekolah', '35000', '40000', 50),
(108, 'Maju Jaya', 'Seragam', 'baju koko', 'seragam sekolah', '30000', '35000', 50),
(109, 'Maju Jaya', 'Seragam', 'rok abu-abu', 'seragam sekolah', '25000', '20000', 49),
(110, 'Maju Jaya', 'Seragam', 'celana abu-abu', 'seragam sekolah', '20000', '25000', 50),
(111, 'Maju Jaya', 'Seragam', 'rok pramuka', 'seragam sekolah', '25000', '30000', 50),
(112, 'Maju Jaya', 'Seragam', 'celana pramuka', 'seragam sekolah', '20000', '25000', 50),
(113, 'Maju Jaya', 'Seragam', 'rok hitam', 'seragam sekolah', '25000', '30000', 50),
(114, 'Maju Jaya', 'Seragam', 'celana hitam', 'seragam sekolah', '20000', '25000', 50),
(115, 'Maju Jaya', 'Seragam', 'jilbab putih', 'seragam sekolah', '10000', '15000', 49),
(116, 'Maju Jaya', 'Seragam', 'jilbab pramuka', 'seragam sekolah', '10000', '15000', 50),
(201, 'Maju Jaya', 'Atribut Seragam', 'topi abu-abu', 'atribut seragam', '7000', '10000', 97),
(202, 'Maju Jaya', 'Atribut Seragam', 'dasi abu-abu', 'atribut seragam', '4000', '5000', 100),
(203, 'Maju Jaya', 'Atribut Seragam', 'kacu', 'atribut seragam', '2500', '3000', 55),
(204, 'Maju Jaya', 'Atribut Seragam', 'topi pamuka', 'atribut seragam', '3000', '4000', 67),
(205, 'Maju Jaya', 'Atribut Seragam', 'peci', 'atribut seragam', '7000', '10000', 60),
(206, 'Maju Jaya', 'Atribut Seragam', 'tali pramuka', 'atribut seragam', '4500', '5000', 56),
(207, 'Maju Jaya', 'Atribut Seragam', 'lambang jurusan perbankan', 'atribut seragam', '1000', '1500', 44),
(208, 'Maju Jaya', 'Atribut Seragam', 'lambang jurusan akuitansi', 'atribut seragam', '1000', '1500', 67),
(209, 'Maju Jaya', 'Seragam', 'lambang jurusan tkj', 'atribut seragam', '1000', '1500', 56),
(210, 'Maju Jaya', 'Atribut Seragam', 'lambang jurusan administr', 'atribut seragam', '1000', '1500', 67),
(211, 'Maju Jaya', 'Atribut Seragam', 'bintang kelas 1', 'atribut seragam', '1000', '1500', 133),
(212, 'Maju Jaya', 'Atribut Seragam', 'bintang kelas II', 'atribut seragam', '1000', '1500', 56),
(213, 'Maju Jaya', 'Atribut Seragam', 'bintang kelas III', 'atribut seragam', '1000', '1500', 45);

-- --------------------------------------------------------

--
-- Table structure for table `sementara`
--

CREATE TABLE IF NOT EXISTS `sementara` (
  `nota` int(3) NOT NULL,
  `barcode` int(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `harga` varchar(15) NOT NULL,
  `jumlah` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sementara`
--

INSERT INTO `sementara` (`nota`, `barcode`, `nama`, `harga`, `jumlah`) VALUES
(1, 103, 'baju pramuka perempuan', '40000', 1),
(1, 101, 'seragam putih perempuan', '40000', 2),
(1, 101, 'seragam putih perempuan', '40000', 2),
(1, 101, 'seragam putih perempuan', '40000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE IF NOT EXISTS `suplier` (
  `id_suplier` int(15) NOT NULL,
  `nama_suplier` varchar(25) NOT NULL,
  `alamat` varchar(35) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `alamat`) VALUES
(1, 'Sederhana', 'padang panjang'),
(2, 'Maju Jaya', 'Padang');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(2) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_pass` varchar(32) NOT NULL,
  `user_level` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_pass`, `user_level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'kepala sekolah', '09d1999c9d753417f55d818048c335a6', 2),
(3, 'kasir', 'c7911af3adbd12a035b289556d96470a', 3),
(4, 'gudang', '202446dd1d6028084426867365b0c7a1', 4),
(5, 'gudang1', '202446dd1d6028084426867365b0c7a1', 4),
(6, 'kasir2', 'c7911af3adbd12a035b289556d96470a', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangmasuk`
--
ALTER TABLE `barangmasuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangmasuk`
--
ALTER TABLE `barangmasuk`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id_suplier` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
