<?php include "../inc/koneksi.php"; 
if (isset($_GET['a']) && $_GET['a'] == 'logout') {
    unset($_SESSION['user_id']);
}
if (!isset($_SESSION['user_id'])&& empty($_SESSION['user_id'])) {
  header ("location:../login.html");
}
?>
<!DOCTYPE html>
<html>

<head> 

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="/image/png" href="../img/logo kps 2.png">

    <title>Business Center SMK Negeri 1 Baso</title>

    <!-- Core CSS - Include with every page -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="listbarang.php">Gudang</a>
            </div>
            <!-- /.navbar-header -->

            

             <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                           <li>
                         <a href="inputbarang.php"><i class="fa fa-table fa-fw"></i>Input Barang</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="listbarang.php?a=logout"><i class="fa fa-sign-out fa-fw"></i>Keluar</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">List Barang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">

                
                <div class="col-lg-12">
                
                <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List Barang
                        </div>
                        <!-- /.panel-heading -->

 						
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nama</th>
                                            <th>Kategori </th>
                                            <th>Suplier </th>
                                            <th>Deskripsi</th>
                                            <th>Stok</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sql = mysql_query("SELECT * FROM produk");
                        while ($data = mysql_fetch_array($sql)) { ?>
                                        <tr >
                                            <td><?php echo $data['id_produk'] ?></td>       
                                            <td><?php echo $data['nama_produk'] ?></td>
                                            <td><?php echo $data['kategori'] ?></td>
                                            <td><?php echo $data['suplier'] ?></td>
                                            <td><?php echo $data['deskripsi']?></td>
                                            <td><?php echo $data['stok']?></td>
                                            <td><a href="hapusbarang.php?id_produk=<?php echo $data['id_produk']; ?>"class="btn btn-ksa">Hapus</a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    
                                </table>
                     
                            </div>
                            <form>
                            
</form>

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
