<?php include "../inc/koneksi.php"; 
if (isset($_GET['a']) && $_GET['a'] == 'logout') {
    unset($_SESSION['user_id']);
}
if (!isset($_SESSION['user_id'])&& empty($_SESSION['user_id'])) {
  header ("location:../login.html");
}
?>
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="/image/png" href="../img/logo kps 2.png">

    <title>Business Center SMK Negeri 1 Baso</title>

    <!-- Core CSS - Include with every page -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="jual.php">Kasir</a>
            </div>
            <!-- /.navbar-header -->

            

           <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                    <li>
                            <a href="changepass.php?id=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-edit fa-fw"></i>Tukar Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="jual.php?a=logout"><i class="fa fa-sign-out fa-fw"></i>Keluar</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Input Barang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           Data Barang
                        </div>
                        <!-- /.panel-heading -->

 						
                        <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <form role="form" method="post" action="bayar.php" enctype="multipart/form-data">
                    <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="barcode1">Barcode Barang 1</label>
                                <input class="form-control" type="text" name="barcode1" placeholder="Barcode">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="jumlah1">Jumlah</label>
                                <input class="form-control" type="text" name="jmlh1" placeholder="Jumlah">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="barcode2">Barcode Barang 2</label>
                                <input class="form-control" type="text" name="barcode2" placeholder="Barcode">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="jumlah2">Jumlah</label>
                                <input class="form-control" type="text" name="jmlh2" placeholder="Jumlah">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="barcode3">Barcode Barang 3</label>
                                <input class="form-control" type="text" name="barcode3" placeholder="Barcode">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="jumlah3">Jumlah</label>
                                <input class="form-control" type="text" name="jmlh3" placeholder="Jumlah">
                            </div>
                        </div>     
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="barcode4">Barcode Barang 4</label>
                                <input class="form-control" type="text" name="barcode4" placeholder="Barcode">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="jumlah4">Jumlah</label>
                                <input class="form-control" type="text" name="jmlh4" placeholder="Jumlah">
                            </div>
                        </div>  
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="barcode5">Barcode Barang 5</label>
                                <input class="form-control" type="text" name="barcode5" placeholder="Barcode">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 floating-label-form-group">

                                <label for="jumlah5">Jumlah</label>
                                <input class="form-control" type="text" name="jmlh5" placeholder="Jumlah">
                            </div>
                        </div>                  
                     <button type="submit" name="submit" class="btn btn-lg btn-ksa">Send</button>
                     <button type="reset" class="btn btn-lg btn-ksa">Reset</button>
                            </div>
                            </form>
    </div>
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
