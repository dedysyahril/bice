<?php include "../inc/koneksi.php"; 
$hasilSimpan = false;
if(isset($_SESSION['data_sementara']) && !empty($_SESSION['data_sementara'])){
    $strInserPenjualan = "INSERT INTO penjualan(nota,waktu,pembayaran,kasir) VALUES(
            ".$_SESSION['data_single']['nota'].",
            NOW(),
            ".$_SESSION['data_single']['total'].",
            ".$_SESSION['user_name']."
        )";
    mysql_query($strInserPenjualan);
    $strDtPrdk = "";
    foreach ($_SESSION['data_sementara'] as $dtPrdk) {
        $strDtPrdk .= (empty($strDtPrdk) ? "INSERT INTO detail(nota,id_produk,jumlah,subtotal) values" : ',').
                    "(
                        '".$_SESSION['data_single']['nota']."',
                        '".$dtPrdk['id_produk']."',
                        ".$dtPrdk['jumlah'].",
                        ".$dtPrdk['subtotal']."
                    )";
    }
    if(!empty($strDtPrdk)){
        mysql_query($strDtPrdk);    
    }
    unset($_SESSION['data_sementara']);
    unset($_SESSION['data_single']);
    $hasilSimpan = true;
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="/image/png" href="../img/logo kps 2.png">

    <title>Business Center</title>

    <!-- Core CSS - Include with every page -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Tables -->
    <link href="../css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="jual.php">Kasir</a>
            </div>
            <!-- /.navbar-header -->

            

             <div class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                   <ul class="nav" id="side-menu">
                    <li>
                            <a href="changepass.php?id=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-edit fa-fw"></i>Tukar Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="jual.php?a=logout"><i class="fa fa-sign-out fa-fw"></i>Keluar</a>
                        </li>
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Result</h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php
                if($hasilSimpan){
                    echo 'Berhasil simpan data';
                }else{
                    echo 'Gagal simpan';
                } ?>
                <br><br>
                <a href="penjualan.php" class="btn btn-lg btn-ksa">Kembali</a>
 <!-- /.row -->
           
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="js/demo/dashboard-demo.js"></script>

</body>

</html>

